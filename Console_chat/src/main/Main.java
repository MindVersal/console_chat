/**
 * 
 */
package main;

import java.util.Scanner;

import client.Client;
import server.Server;

/**
 * @author isp_pia
 *
 */
public class Main {

	private static Scanner in;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Programm for Console Chat.");
		in = new Scanner(System.in);
		boolean stateNow = true;
		
		System.out.println("Start program what is Client press: (C), " + 
				"is Server press: (S), for Exit press (E).");
		char answer;
		do {
			answer = Character.toLowerCase(in.nextLine().charAt(0));
			switch (answer) {
			case 's':
				System.out.println("Start as Server.");
				new Server();
				break;
			case 'c':
				System.out.println("Start as Client.");
				new Client();
				break;
			case 'x':
				System.out.println("Exit from program.");
				stateNow = false;
				break;
			default:
				System.out.println("Uncorrect. Please, " + 
						"for start program what is Client press: (C), " + 
						"is Server press: (S), for Exit press (E).");
				break;
			}
		} while ( stateNow );
		System.out.println("Stop server.");
		System.out.println("The End.");
	}
}
