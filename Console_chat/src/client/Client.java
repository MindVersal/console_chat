/**
 * 
 */
package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import main.Const;

/**
 * ������������ ������ ��������� � ������ �������
 * 
 * @author isp_pia
 *
 */
public class Client {
	private BufferedReader in;
	private PrintWriter out;
	private Socket socket;
	private Scanner scan;
	/**
	 * ����������� � ������������ ��� � �������������� ����� ����������� �
	 * ��������
	 */
	public Client() {
		scan = new Scanner(System.in);
		System.out.println("Insert IP for connect to server.");
		System.out.println("Format: xxx.xxx.xxx.xxx");
		String ip = scan.nextLine();
		try {
			socket = new Socket(ip, Const.PORT);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			System.out.println("Insert your NicName:");
			out.println(scan.nextLine());
			
			Resender resend = new Resender();
			resend.start();
			
			String str = "";
			while ( !str.trim().toLowerCase().equals("exit") ) {
				str = scan.nextLine();
				out.println(str);
			}
			resend.setStop();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close();
		}
	}
	/**
	 * ��������� ������� � �������� ������ � �����
	 */
	private void close(){
		try {
			in.close();
			out.close();
			socket.close();
		} catch (Exception e) {
			System.err.println("������ �� ���� �������!");
		}
	}
	/**
	 * ����� � ��������� ���� ���������� ��� ��������� �� ������� � �������.
	 * �������� ���� �� ����� ������ ����� setStop().
	 * 
	 * @author isp_pia
	 */
	private class Resender extends Thread {
		private boolean stoped;
		/**
		 * ���������� ��������� ���������
		 */
		public void setStop(){
			stoped = true;
		}
		/**
		 * ��������� ��� ��������� �� ������� � �������� �� � �������.
		 * ��������������� ������� ������ setStop()
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			try {
				while (!stoped) {
					String str = in.readLine();
					System.out.println(str);
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Error for get message.");
				e.printStackTrace();
			}
		}
	}
}
